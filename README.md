# README #

BitAntenna/Twitter プロジェクト

### 作業 ###

1. テストサーバー(153.120.4.36)への接続をする
    * Port: 61203
    * root / パスワードいつもの
2. BitAntennaをTwitter Developerに登録して、consumer key/secret access key/secretを手に入れる
3. Twitter/Ruby Libraryを入れて、自動投稿できるようにする
    * root/bitantenna/bitantenna_rubyで作業中
4. BitBucketに新しいアカウントを作り、Repoを作る
    * <https://bitbucket.org/mitsurutk/bitantenna_ruby>
    * SSH設定 (OK??)
5. 俺、戸田、石橋を招待する。
6. 俺から送られるコード(Blockchainからビットコインの価格自動取得)をpushする
7. そのコードと3.のコードをがっちゃんこする
8. 時間設定して、30分に一度とか1時間に一度価格のアップデートをする
